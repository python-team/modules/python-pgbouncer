************************************************
python-pgbouncer: Python glue to drive pgbouncer
************************************************

    Copyright (c) 2011, Canonical Ltd

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
python-pgbouncer provides a python interface to setup and run a pgbouncer
instance.

Dependencies
============

* Python 3.5+

* pgbouncer

* psycopg2 (https://pypi.org/project/psycopg2)

* python-fixtures (https://launchpad.net/python-fixtures or
  https://pypi.org/project/fixtures)

* testtools (https://pypi.org/project/testtools)

Testing Dependencies
====================

In addition to the above, the tests also depend on:

* postgresfixture (https://pypi.org/project/postgresfixture)

* testscenarios (https://pypi.org/project/testscenarios)

Usage
=====

Create a PGBouncerFixture - a context manager with an extended protocol
supporting access to logs etc. Customise it with database definitions, user
credentials, and then when you enter the context it will create a transient
pgbouncer setup in a temporary directory and run it for the duration that the
context is open.

For instance::

 >>> from pgbouncer import PGBouncerFixture
 >>> bouncer = PGBouncerFixture()
 >>> bouncer.databases['mydb'] = 'host=hostname dbname=foo'
 >>> bouncer.users['user1'] = 'credentials'
 >>> with bouncer:
 ...     # Can now connect to bouncer.host port=bouncer.port user=user1

Any settings required for pgbouncer to work will be given sane defaults.


Installation
============

Either run setup.py in an environment with all the dependencies available, or
add the working directory to your PYTHONPATH.


Development
===========

Upstream development takes place at https://launchpad.net/python-pgbouncer.

To run the tests, run:

 $ tox
